﻿using SimpleRetailApp.Models.Goods;
using System;
using System.Collections.ObjectModel;

namespace SimpleRetailApp.ViewModels.Goods
{
    public sealed class GoodDetailsViewModel : BaseViewModel
    {
        public GoodDetails SelectedGood;

        public GoodDetailsViewModel()
        {
            SelectedGood = new GoodDetails();
        }

        public GoodDetailsViewModel(GoodDetails editingItem)
        {
            SelectedGood = editingItem;
        }

        public string LabelName 
        {
            get => SelectedGood.LabelName;
            set
            {
                if(string.IsNullOrWhiteSpace(value))
                {
                    throw new ArgumentNullException(nameof(LabelName), "Відсутня назва для етикетки");
                }

                SelectedGood.LabelName = value;
                OnPropertyChanged(nameof(LabelName));

                if (string.IsNullOrEmpty(SelectedGood.FullName))
                {
                    FullName = value;
                }
            }
        }
        public string FullName
        {
            get => SelectedGood.FullName;
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    SelectedGood.FullName = SelectedGood.LabelName;
                }
                else
                {
                    SelectedGood.FullName = value;
                }
                OnPropertyChanged(nameof(FullName));
            }
        }
        public string Description
        {
            get => SelectedGood.Description;
            set
            {
                SelectedGood.Description = value;
                OnPropertyChanged(nameof(Description));
            }
        }
        public decimal PurchasePrice
        {
            get => SelectedGood.PurchasePrice;
            set
            {
                if(value <= 0.0m)
                {
                    throw new ArgumentException("Ціна не може бути менше/рівною нулю", nameof(PurchasePrice));
                }

                SelectedGood.PurchasePrice = value;
                OnPropertyChanged(nameof(PurchasePrice));
            }
        }
        public decimal SellPrice
        {
            get => SelectedGood.SellPrice;
            set
            {
                if (value <= 0.0m)
                {
                    throw new ArgumentException("Ціна не може бути менше/рівною нулю", nameof(SellPrice));
                }

                SelectedGood.SellPrice = value;
                OnPropertyChanged(nameof(SellPrice));
            }
        }
        public ObservableCollection<GoodBarcode> Barcodes
        {
            get => SelectedGood.Barcodes;
            set
            {
                SelectedGood.Barcodes = value;
                OnPropertyChanged(nameof(Barcodes));
            }
        }
    }
}
