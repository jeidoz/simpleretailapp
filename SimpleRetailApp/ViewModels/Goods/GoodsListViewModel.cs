﻿using SimpleRetailApp.Models.Goods;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows.Data;

namespace SimpleRetailApp.ViewModels.Goods
{
    public sealed class GoodsListViewModel : BaseViewModel
    {
        private string _searchField;
        private ICollectionView _goods;
        private ICollection<GoodsListItem> _goodsCollection;

        public GoodsListViewModel()
        {
            _searchField = string.Empty;
            GoodsCollection = new List<GoodsListItem>(GetTestGoods);
            Goods = CollectionViewSource.GetDefaultView(GoodsCollection);
            Goods.Filter = new Predicate<object>(Filter);
        }
        private IEnumerable<GoodsListItem> GetTestGoods
        {
            get
            {
                yield return new GoodsListItem
                {
                    LabelName = "Капсула Vizir",
                    PurchasePrice = 3m,
                    SellPrice = 8m,
                    Barcodes = new ObservableCollection<GoodBarcode>
                    {
                        new GoodBarcode { Barcode = "1234567890" },
                        new GoodBarcode { Barcode = "0987654321" }
                    }
                };
                yield return new GoodsListItem
                {
                    LabelName = "Олівець звичайний",
                    PurchasePrice = 0.33m,
                    SellPrice = 1.25m,
                    Barcodes = new ObservableCollection<GoodBarcode>
                    {
                        new GoodBarcode { Barcode = "7776667770" }
                    }
                };
            }
        }
        private bool Filter(object obj)
        {
            if (obj is GoodsListItem data)
            {
                if (!string.IsNullOrEmpty(_searchField))
                {
                    return data.LabelName.Contains(_searchField)
                        || data.Barcodes.Any(barcode => barcode.Barcode.Contains(_searchField));
                }
                return true;
            }
            return false;
        }

        public string SearchField
        {
            get => _searchField;
            set
            {
                _searchField = value;
                OnPropertyChanged(nameof(SearchField));
                FilterCollection();
            }
        }
        private void FilterCollection()
        {
            if (_goods != null)
            {
                _goods.Refresh();
            }
        }

        public ICollectionView Goods
        {
            get => _goods;
            set 
            {
                _goods = value;
                OnPropertyChanged(nameof(Goods));
            }
        }

        public ICollection<GoodsListItem> GoodsCollection
        {
            get => _goodsCollection;
            set
            {
                _goodsCollection = value;
                OnPropertyChanged(nameof(GoodsCollection));
            }
        }

        public void AddGood(GoodsListItem newGood)
        {
            _goodsCollection.Add(newGood);
            OnPropertyChanged(nameof(GoodsCollection));
            Goods = CollectionViewSource.GetDefaultView(_goodsCollection);
            Goods.Refresh();
        }
    }
}