﻿using SimpleRetailApp.Models.Tabs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleRetailApp.ViewModels.Tabs
{
    public sealed class ActionTabViewModel
    {
        public ObservableCollection<ActionTabItem> Tabs { get; set; }

        public ActionTabViewModel()
        {
            Tabs = new ObservableCollection<ActionTabItem>();
        }
    }
}
