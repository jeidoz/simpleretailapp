﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace SimpleRetailApp.ViewModels
{
    public abstract class BaseViewModel : INotifyPropertyChanged
    {
        #region Notify Property

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}