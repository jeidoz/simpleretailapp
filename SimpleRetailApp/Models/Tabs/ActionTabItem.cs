﻿using System.Windows.Controls;

namespace SimpleRetailApp.Models.Tabs
{
    public sealed class ActionTabItem
    {
        public string Header { get; set; }
        public UserControl Content { get; set; }
    }
}