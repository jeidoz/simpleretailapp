﻿using System.Collections.ObjectModel;

namespace SimpleRetailApp.Models.Goods
{
    public class GoodsListItem
    {
        public GoodsListItem()
        {
            Barcodes = new ObservableCollection<GoodBarcode>();
        }

        public int Id { get; set; }
        public string LabelName { get; set; }
        public ObservableCollection<GoodBarcode> Barcodes { get; set; }
        public decimal PurchasePrice { get; set; }
        public decimal SellPrice { get; set; }
    }
}