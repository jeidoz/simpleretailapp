﻿namespace SimpleRetailApp.Models.Goods
{
    public sealed class GoodDetails : GoodsListItem
    {
        public string FullName { get; set; }
        public string Description { get; set; }
    }
}