﻿namespace SimpleRetailApp.Models.Goods
{
    public sealed class GoodBarcode
    {
        public int Id { get; set; }
        public string Barcode { get; set; }
    }
}