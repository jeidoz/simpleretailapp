﻿using SimpleRetailApp.ViewModels.Goods;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SimpleRetailApp.Controls.Goods
{
    /// <summary>
    /// Interaction logic for GoodsListControl.xaml
    /// </summary>
    public partial class GoodsListControl : UserControl
    {
        public static readonly RoutedUICommand NewGood = new RoutedUICommand(
                name: "NewGood",
                text: "Добавити товар",
                ownerType: typeof(GoodsListControl),
                inputGestures: new InputGestureCollection
                {
                    new KeyGesture(Key.Insert)
                });

        private readonly GoodsListViewModel _viewModel;

        public GoodsListControl()
        {
            InitializeComponent();
            _viewModel = new GoodsListViewModel();
            DataContext = _viewModel;
        }

        private void AddGoodExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            var addGoodWnd = new AddGoodWindow();
            if(addGoodWnd.ShowDialog() == true)
            {
                _viewModel.AddGood(addGoodWnd.NewGood);
            }
        }

        private void AddGoodCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = e.Source is GoodsListControl;
        }
    }
}