﻿using SimpleRetailApp.Models.Goods;
using SimpleRetailApp.ViewModels.Goods;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SimpleRetailApp.Controls.Goods
{
    /// <summary>
    /// Interaction logic for AddGoodWindow.xaml
    /// </summary>
    public partial class AddGoodWindow : Window
    {
        private readonly GoodDetailsViewModel _viewModel;

        public GoodDetails NewGood { get => _viewModel.SelectedGood; }

        public AddGoodWindow()
        {
            _viewModel = new GoodDetailsViewModel();
            this.DataContext = _viewModel;
            InitializeComponent();
        }

        private void AddGood_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            Close();
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }
    }
}
