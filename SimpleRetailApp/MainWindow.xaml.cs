﻿using SimpleRetailApp.Controls;
using SimpleRetailApp.Controls.Goods;
using SimpleRetailApp.Models.Tabs;
using SimpleRetailApp.ViewModels.Tabs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SimpleRetailApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ActionTabViewModel _viewModel;

        public MainWindow()
        {
            InitializeComponent();
            _viewModel = new ActionTabViewModel();
            ActionTabs.ItemsSource = _viewModel.Tabs;
        }

        private void TabCross_MouseDown(object sender, MouseButtonEventArgs e)
        {
            _viewModel.Tabs.RemoveAt(ActionTabs.SelectedIndex);
        }

        private void OpenGoodsList_Click(object sender, RoutedEventArgs e)
        {
            var newTab = new ActionTabItem
            {
                Header = "Номенклатура",
                Content = new GoodsListControl()
            };
            _viewModel.Tabs.Add(newTab);
            ActionTabs.SelectedIndex = _viewModel.Tabs.Count - 1;
        }
    }
}
