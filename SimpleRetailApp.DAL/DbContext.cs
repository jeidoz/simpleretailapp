﻿using SimpleRetailApp.DAL.Repositories;

namespace SimpleRetailApp.DAL
{
    public static class DbContext
    {
        private const string _conectionString = "Database.db3";
        private static GoodsRepository _goodsRepo;
        private static GoodBarcodesRepository _barcodesRepo;

        public static GoodBarcodesRepository Barcodes
        {
            get
            {
                if (_barcodesRepo == null)
                {
                    _barcodesRepo = new GoodBarcodesRepository(_conectionString);
                }
                return _barcodesRepo;
            }
        }
        public static GoodsRepository Goods
        {
            get
            {
                if(_goodsRepo == null)
                {
                    _goodsRepo = new GoodsRepository(_conectionString);
                }
                return _goodsRepo;
            }
        }
    }
}