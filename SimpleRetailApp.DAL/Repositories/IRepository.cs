﻿using SimpleRetailApp.DAL.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SimpleRetailApp.DAL.Repositories
{
    public interface IRepository<T> where T : BaseEntity
    {
        Task<T> GetAsync(int id);
        Task<List<T>> GetAllAsync();
        Task<int> SaveAsync(T entity);
        Task<int> DeleteAsync(T entity);
    }
}