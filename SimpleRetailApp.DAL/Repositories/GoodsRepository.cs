﻿using SimpleRetailApp.DAL.Entities.Goods;
using SQLite;
using SQLiteNetExtensions.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SimpleRetailApp.DAL.Repositories
{
    public sealed class GoodsRepository : IRepository<Good>
    {
        private readonly SQLiteAsyncConnection _database;

        public GoodsRepository(string dbPath)
        {
            _database = new SQLiteAsyncConnection(dbPath);
            _database.CreateTableAsync<GoodBarcode>().Wait();
            _database.CreateTableAsync<Good>().Wait();
        }

        public async Task<List<Good>> GetAllAsync()
        {
            var allGoods = await _database.Table<Good>().ToListAsync();
            allGoods.ForEach(async (good) => await DbContext.Barcodes.GetAllAsync(good.Id));
            return allGoods;
        }

        public async Task<Good> GetAsync(int id)
        {
            var requestedGood = await _database.Table<Good>()
                .Where(good => good.Id == id)
                .FirstOrDefaultAsync();
            requestedGood.Barcodes = await DbContext.Barcodes.GetAllAsync(requestedGood.Id);
            return requestedGood;
        }

        public Task<int> SaveAsync(Good good)
        {
            good.Barcodes.ForEach(async (barcode) => await DbContext.Barcodes.SaveAsync(barcode));
            if (good.Id != 0)
            {
                return _database.UpdateAsync(good);
            }
            else
            {
                return _database.InsertAsync(good);
            }
        }

        public Task<int> DeleteAsync(Good good)
        {
            return _database.DeleteAsync(good);
        }
    }
}