﻿using SimpleRetailApp.DAL.Entities.Goods;
using SimpleRetailApp.DAL.Exceptions;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SimpleRetailApp.DAL.Repositories
{
    public sealed class GoodBarcodesRepository : IRepository<GoodBarcode>
    {
        private readonly SQLiteAsyncConnection _database;

        public GoodBarcodesRepository(string dbPath)
        {
            _database = new SQLiteAsyncConnection(dbPath);
            _database.CreateTableAsync<GoodBarcode>().Wait();
        }

        public Task<List<GoodBarcode>> GetAllAsync()
        {
            return _database.Table<GoodBarcode>().ToListAsync();
        }

        public Task<List<GoodBarcode>> GetAllAsync(int goodId)
        {
            return _database.Table<GoodBarcode>()
                .Where(barcode => barcode.GoodId == goodId)
                .ToListAsync();
        }

        public Task<GoodBarcode> GetAsync(int id)
        {
            return _database.Table<GoodBarcode>()
                .Where(barcode => barcode.Id == id)
                .FirstOrDefaultAsync();
        }

        public async Task<int> SaveAsync(GoodBarcode barcode)
        {
            if (barcode.Id != 0)
            {
                return await _database.UpdateAsync(barcode);
            }
            else if ((await GetAllAsync()).Exists(code => code.Barcode == barcode.Barcode))
            {
                throw new NotUniqueBarcodeException();
            }
            else
            {
                return await _database.InsertAsync(barcode);
            }
        }

        public Task<int> DeleteAsync(GoodBarcode barcode)
        {
            return _database.DeleteAsync(barcode);
        }
    }
}
