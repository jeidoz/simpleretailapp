﻿using System;

namespace SimpleRetailApp.DAL.Exceptions
{
    public sealed class NotUniqueBarcodeException : Exception
    {
        public NotUniqueBarcodeException(string message = "Given barcode is not unique.")
            : base(message)
        {
        }
    }
}