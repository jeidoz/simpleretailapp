﻿using SQLiteNetExtensions.Attributes;
using System.Collections.Generic;

namespace SimpleRetailApp.DAL.Entities.Goods
{
    public abstract class BaseGood : BaseEntity
    {
        public string LabelName { get; set; }
        [OneToMany]
        public List<GoodBarcode> Barcodes { get; set; } = new List<GoodBarcode>();
        public int QuantityAvailable { get; set; }
        public decimal SellPrice { get; set; }
    }
}