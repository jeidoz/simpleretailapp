﻿using SQLite;

namespace SimpleRetailApp.DAL.Entities.Goods
{
    [Table("Goods")]
    public sealed class Good : BaseGood
    {
        public string FullName { get; set; }
        public string Comment { get; set; }
        public int MinimumAmount { get; set; }
        public decimal PurchasePrice { get; set; }
    }
}