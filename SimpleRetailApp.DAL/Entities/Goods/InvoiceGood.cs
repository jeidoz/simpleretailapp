﻿using SimpleRetailApp.DAL.Entities.Documents;
using SQLite;
using SQLiteNetExtensions.Attributes;

namespace SimpleRetailApp.DAL.Entities.Goods
{
    [Table("InvoiceGoods")]
    public sealed class InvoiceGood : BaseGood
    {
        [ForeignKey(typeof(Invoice))]
        public int InvoiceId { get; set; }
        public int ListOrder { get; set; }
        public int Quantity { get; set; }
    }
}