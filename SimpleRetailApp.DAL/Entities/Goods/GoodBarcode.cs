﻿using SQLite;
using SQLiteNetExtensions.Attributes;

namespace SimpleRetailApp.DAL.Entities.Goods
{
    [Table("GoodBarcodes")]
    public sealed class GoodBarcode : BaseEntity
    {
        [ForeignKey(typeof(Good))]
        public int GoodId { get; set; }
        [Unique]
        public string Barcode { get; set; }
    }
}