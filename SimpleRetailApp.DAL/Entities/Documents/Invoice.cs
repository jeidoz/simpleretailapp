﻿using SimpleRetailApp.DAL.Entities.Goods;
using SQLite;
using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;

namespace SimpleRetailApp.DAL.Entities.Documents
{
    [Table("Invoices")]
    public sealed class Invoice : BaseEntity
    {
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        [OneToMany]
        public List<InvoiceGood> SelectedGoods { get; set; }
        public decimal Sum { get; set; }
    }
}